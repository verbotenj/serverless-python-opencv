import boto3
import cv2
import numpy as np
import uuid
import os
from utils import convert_to_gray

s3Client = boto3.client('s3')


def grayscale(event, context):
    print("S3 file was uploaded")
    bucketName = event['Records'][0]['s3']['bucket']['name']
    bucketKey = event['Records'][0]['s3']['object']['key']

    download_path = '/tmp/{}{}'.format(uuid.uuid4(), bucketKey)
    output_path = '/tmp/{}'.format(bucketKey)

    s3Client.download_file(bucketName, bucketKey, download_path)

    try:
        print(download_path)
        img = cv2.imread(download_path)
        gray = convert_to_gray.convert_to_gray(img)
        cv2.imwrite(output_path, gray)
    except Exception as e:
        print(e)
        print('Error processing file with OpenCV')
        raise e
    try:
        s3Client.upload_file(
            output_path, os.environ['OPENCV_OUTPUT_BUCKET'], bucketKey)
    except Exception as e:
        print(e)
        print('Error uploading file to output bucket')
        raise e
    return bucketKey
