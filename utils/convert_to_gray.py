import cv2
import numpy as np


def main():
    image = cv2.imread('in/teapad.jpeg')
    image_gray = convert_to_gray(image)
    cv2.imshow('Gray image', image_gray)
    cv2.waitKey(1000)
    cv2.imwrite('out/teapad-gray.jpeg', image_gray)


def convert_to_gray(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


if __name__ == "__main__":
    main()

