import utils.convert_to_gray as utils
import cv2


def test_is_image_gray():
    image = cv2.imread('in/teapad.jpeg')
    image_gray = utils.convert_to_gray(image)
    print(image_gray.shape)
    size_gray = len(image_gray.shape)
    assert (size_gray < 3)
