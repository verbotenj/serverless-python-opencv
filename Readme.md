# Serverless, Lambda, OpenCV

## Description

Project converts color image to grayscale image. It uses S3 as trigger event for lambda function.

Stack: Serverless, AWS Lambda, python, OpenCV

## Try it out

Setup project

- setup serverless framework
- setup aws cli
- git clone ...
- change bucket name in serverless.yml (look hints)
- cd bucket `serverless deploy resources.yml`
- cd to root `serverless deploy`

Test project

- copy picture to the S3 bucket
  `aws s3 cp in/teapad.jpeg s3://your-bucket-name`
- download image from S3 bucket
- picture downloaded picture should be grayscale

## Steps to setup project from ground up to test it locally and deploy on AWS Lambda

Create project from template
`serverless create -t aws-python3 -p aws-serverless-opencv`

If you created your project folder already, you could cd into the folder before running the mkvirtualenv command with some arguments to automatically navigate to the project folder whenever you activate the virtual environment. Here is the format: mkvirtualenv [-a project_path][-i package] [-r requirements_file][virtualenv options] [ENVNAME]

`mkvirtualenv -a $(pwd) py3cv4`

Additional commands for help if you get lost
`lsvirtualenv` - list virtual environments
`workon [nameOfenv]` - activate virtual env
`deactivate` - stop using virtual env
`pip list` - list of installed packages

Install python packages for local setup. It will install opencv and numpy for us.
`pip install opencv-contrib-python`

Create requirements file to tell serverless what are our extra requirements. We setup serverless.yml to use separate layer for requirements, because lambda stays small and we can reference to the layer from other lambda functions.

```requirements.txt
opencv-python==4.1.0.25
numpy==1.16.3
```

We would generally use `pip freeze > requirements.txt` to install from them, but lambda functions come with basic modules already.

Run python tests
`pytest`
more informational tests
`pytest -vv`

Init project to create packages.json
`npm init`

Node modules install

```bash
npm i -D serverless-offline serverless-python-requirements

```

## Hint

- Deploy might fail if bucket name is already in use - Aws S3 rule (bucket name has to be **globally unique**)
- You need docker running on non-linux setup for `sls deploy`

## Known Issues

Doesn't work

- runtime: python3.8 (import module cv2.cv2)
- open-contrib-python: 4.2.0.34
